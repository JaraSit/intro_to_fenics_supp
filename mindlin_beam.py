from fenics import*
import matplotlib.pyplot as plt

# --------------------
# Parameters
# --------------------
E = 50.0e9
G = 25.0e9
b = 0.1
h = 0.01
Ar = b*h
I = (1.0/12.0)*b*h*h*h

k = 1
f_lin = 1.0
n = 80000
l = 2.0
F = 1.0

# --------------------
# Define geometry
# --------------------
mesh = IntervalMesh(n, 0, l)

# --------------------
# Define spaces
# --------------------
P1 = FiniteElement('P', interval, 2)
element = MixedElement([P1, P1, P1])
V = FunctionSpace(mesh, element)
W = FunctionSpace(mesh, 'P', 2)

d_u, d_w, d_phi = TestFunctions(V)
u, w, phi = TrialFunctions(V)
u_ = Function(V)

# --------------------
# Boundary conditions
# --------------------
bc_u = Constant(0.0)

tol = 1e-14
def left_end(x):
	return near(x[0], 0.0)

def right_end(x):
	return near(x[0], 2.0)

bc1 = DirichletBC(V.sub(0), Constant(0.0), left_end)
bc2 = DirichletBC(V.sub(1), Constant(0.0), left_end)
bc3 = DirichletBC(V.sub(1), Constant(0.0), right_end)
bc4 = DirichletBC(V.sub(0), Constant(0.0), right_end)
bc = [bc1, bc2, bc3, bc4]

# --------------------
# Initialization
# --------------------
X = Function(V)
f = Function(V)
f.interpolate(Constant((0.0, f_lin, 0.0)))

# --------------------
# Solution
# --------------------
A = d_u.dx(0)*E*Ar*u.dx(0)*dx + \
	d_w.dx(0)*G*Ar*w.dx(0)*dx + d_w.dx(0)*G*Ar*phi*dx + \
	d_phi.dx(0)*E*I*phi.dx(0)*dx + d_phi*G*Ar*(phi + w.dx(0))*dx + \
	Constant(0.0)*d_w*dx

A_ass, b_ass = assemble_system(lhs(A), rhs(A), bc)

pointForce = PointSource(V.sub(1), Point(1.0), 1)
pointForce.apply(b_ass)

solve(A_ass, X.vector(), b_ass)


u_, w_, phi_ = X.split(deepcopy=True)

#plot(w_)
plot(X.sub(1))
plt.show()

file = File("mindlin_beam/mindlin_beam.pvd")
file << w_