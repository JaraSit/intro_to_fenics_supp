from fenics import *
import matplotlib.pyplot as plt

# Variables and constants
lambda_ = 1.25
mu = 1

# Create mesh and define function space
mesh = RectangleMesh(Point(0.0, 0.0), Point(5.0, 5.0), 20, 20)
V = VectorFunctionSpace(mesh, 'P', 1)

# Define boundary condition
tol = 1e-14

def boundary(x, on_boundary):
	return (on_boundary and x[1] < tol)

bc = DirichletBC(V, Constant((0.0, 0.0)), boundary)

# Define strain and stress
def epsilon(u):
	return 0.5*(nabla_grad(u) + nabla_grad(u).T)

def sigma(u):
	return lambda_*div(u)*Identity(d) + 2*mu*epsilon(u)

# Define variational problem
u = TrialFunction(V)
d = u.geometric_dimension()
v = TestFunction(V)
f = Constant((0.0, 1.0))

a = inner(sigma(u), epsilon(v))*dx
L = dot(f, v)*dx

# Compute solution
u = Function(V)
solve(a == L, u, bc)

# Plot solution
plot(u)
plt.show()

# Save solution
file = File("plane_strain/plane_strain.pvd")
file << u