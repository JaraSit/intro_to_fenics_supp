from fenics import *
import matplotlib.pyplot as plt


# --------------------
# Parameters
# --------------------
n = 200
l = 1.0
lc = 0.1
Gc = 0.1

# --------------------
# Define geometry
# --------------------
mesh = IntervalMesh(n, 0.0, l)

# --------------------
# Define spaces & fces
# --------------------
V = FunctionSpace(mesh, 'CG', 1)
s = Function(V)
q = TestFunction(V)
His = Function(V)

# --------------------
# Boundary conditions
# --------------------
def middle_point(x):
	return near(x[0], l/2)

BC =  DirichletBC(V, 0.1, middle_point)

# --------------------
# Nonlinear problem 1
# --------------------
E_ds = -2.0*His*inner((1.0-s), q)*dx + Gc*(q + lc*lc*inner(grad(s), grad(q)))*dx

lower = interpolate(Constant(0.0), V)
upper = interpolate(Constant(1.0), V)

# From DOLFIN Docs:
# UFL form operator: Compute the Gateaux derivative of form w.r.t. 
# coefficient in direction of argument.
# If the argument is omitted, a new Argument is created in the same 
# space as the coefficient, with argument number one higher than 
# the highest one in the form.
# The resulting form has one additional Argument in the same 
# finite element space as the coefficient.

H = derivative(E_ds, s, TrialFunction(V))

snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver": {"linear_solver": "lu",
                          				  "relative_tolerance": 1e-4,
                                          "maximum_iterations": 200,
                                          "report": True,
                                          "error_on_nonconvergence": False}}

problem = NonlinearVariationalProblem(E_ds, s, BC, H)
problem.set_bounds(lower, upper)

solver = NonlinearVariationalSolver(problem)
solver.parameters.update(snes_solver_parameters)
solver.solve()

plot(s)
plt.show()