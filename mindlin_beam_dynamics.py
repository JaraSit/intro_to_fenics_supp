from fenics import*
import matplotlib.pyplot as plt
from matplotlib import animation

# --------------------
# Parameters
# --------------------
E = 50.0e9
G = 25.0e9
b = 0.1
h = 0.01
Ar = b*h
I = (1.0/12.0)*b*h*h*h

k = 1
f_lin = 1.0
n = 100
l = 2.0
F = 1.0
rho = 1.0

end_time = 0.01
tau = 0.0001

# --------------------
# Define geometry
# --------------------
mesh = IntervalMesh(n, 0, l)

# --------------------
# Define spaces
# --------------------
P1 = FiniteElement('P', interval, 2)
element = MixedElement([P1, P1, P1])
V = FunctionSpace(mesh, element)
W = FunctionSpace(mesh, 'P', 2)

d_u, d_w, d_phi = TestFunctions(V)
u, w, phi = TrialFunctions(V)
u_ = Function(V)

# --------------------
# Boundary conditions
# --------------------
bc_u = Constant(0.0)

tol = 1e-14
def left_end(x):
	return near(x[0], 0.0)

def right_end(x):
	return near(x[0], 2.0)

bc1 = DirichletBC(V.sub(0), Constant(0.0), left_end)
bc2 = DirichletBC(V.sub(1), Constant(0.0), left_end)
bc3 = DirichletBC(V.sub(1), Constant(0.0), right_end)
bc4 = DirichletBC(V.sub(0), Constant(0.0), right_end)
bc = [bc1, bc2, bc3, bc4]

# --------------------
# Initialization
# --------------------
X = Function(V)
f = Function(V)
f.interpolate(Constant((0.0, f_lin, 0.0)))
u_bar = Function(V)
du = Function(V)
ddu = Function(V)
ddu_old = Function(V)

A = d_u.dx(0)*E*Ar*u.dx(0)*dx + 4*rho*Ar/(tau*tau)*(d_u*u - d_u*u_bar.sub(0))*dx + \
	d_w.dx(0)*G*Ar*w.dx(0)*dx + d_w.dx(0)*G*Ar*phi*dx + 4*rho*Ar/(tau*tau)*(d_w*w - d_w*u_bar.sub(1))*dx + \
	d_phi.dx(0)*E*I*phi.dx(0)*dx + d_phi*G*Ar*(phi + w.dx(0))*dx + 4*rho*I/(tau*tau)*(d_phi*phi - d_phi*u_bar.sub(2))*dx + \
	Constant(1.0)*d_w*dx

# Create XDMF files for visualization output
xdmffile_u = XDMFFile('mindlin_beam/dynamics.xdmf')

# --------------------
# Solution
# --------------------
t = 0
while(t < end_time):
	u_bar.assign(X + tau*du + 0.25*tau*tau*ddu)

	A_ass, b_ass = assemble_system(lhs(A), rhs(A), bc)

	pointForce = PointSource(V.sub(1), Point(1.0), 0)
	pointForce.apply(b_ass)

	solve(A_ass, X.vector(), b_ass)
	u_, w_, phi_ = X.split(deepcopy=True)

	A = d_u.dx(0)*E*Ar*u.dx(0)*dx + 4*rho*Ar/(tau*tau)*(d_u*u - d_u*u_bar.sub(0))*dx + \
		d_w.dx(0)*G*Ar*w.dx(0)*dx + d_w.dx(0)*G*Ar*phi*dx + 4*rho*Ar/(tau*tau)*(d_w*w - d_w*u_bar.sub(1))*dx + \
		d_phi.dx(0)*E*I*phi.dx(0)*dx + d_phi*G*Ar*(phi + w.dx(0))*dx + 4*rho*I/(tau*tau)*(d_phi*phi - d_phi*u_bar.sub(2))*dx + \
		Constant(0.0)*d_w*dx

	ddu_old.assign(ddu)
	ddu.assign(4/(tau*tau)*(X - u_bar))
	du.assign(du + 0.5*tau*(ddu + ddu_old))

	xdmffile_u.write(w_, t)

	print(t)
	t = t + tau