from fenics import*
import matplotlib.pyplot as plt

# --------------------
# Parameters
# --------------------
E = 50.0e9
G = 25.0e9
b = 0.1
h = 0.01
Ar = b*h
I = (1.0/12.0)*b*h*h*h

k = 1
f_lin = 1.0
n = 100
l = 2.0
F_pre = 5.0

# --------------------
# Define geometry
# --------------------
mesh = IntervalMesh(n, 0, l)

# --------------------
# Define spaces
# --------------------
P1 = FiniteElement('P', interval, 2)
element = MixedElement([P1, P1, P1])
V = FunctionSpace(mesh, element)
W = FunctionSpace(mesh, 'P', 1)

d_u, d_w, d_phi = TestFunctions(V)
u2, w2, phi2 = TrialFunctions(V)
u_ = Function(V)

# --------------------
# Boundary conditions
# --------------------
bc_u = Constant(0.0)

tol = 1e-14
def left_end(x):
	return near(x[0], 0.0)

def right_end(x):
	return near(x[0], 2.0)

bc1 = DirichletBC(V.sub(0), Constant(0.0), left_end)
bc2 = DirichletBC(V.sub(1), Constant(0.0), left_end)
bc3 = DirichletBC(V.sub(1), Constant(0.0), right_end)
bc4 = DirichletBC(V.sub(0), Constant(0.0), right_end)
bc = [bc1, bc2, bc3, bc4]

# --------------------
# Initialization
# --------------------
X = Function(V)
u, w, phi = split(X)
f = Function(W)
f.vector()[n/2] = F_pre*n/l
plot(project(f, W))
plt.show()

# --------------------
# Linear solution
# --------------------
F_lin = d_u.dx(0)*E*Ar*(u2.dx(0))*dx + \
	d_w.dx(0)*G*Ar*w2.dx(0)*dx + d_w.dx(0)*G*Ar*phi2*dx + \
	d_phi.dx(0)*E*I*phi2.dx(0)*dx + d_phi*G*Ar*(phi2 + w2.dx(0))*dx - f*d_w*dx

solve(lhs(F_lin) == rhs(F_lin), X, bc)
u_, w_, phi_ = X.split(deepcopy=True)

plot(w_, c="red")

# --------------------
# Solution
# --------------------
# Nonlinear form
F = d_u.dx(0)*(E*Ar*(u.dx(0) + 0.5*w.dx(0)*w.dx(0)))*dx + \
	d_w.dx(0)*G*Ar*w.dx(0)*dx + d_w.dx(0)*G*Ar*phi*dx + d_w.dx(0)*(E*Ar*u.dx(0)*w.dx(0) + 0.5*E*Ar*w.dx(0)*w.dx(0)*w.dx(0))*dx + \
	d_phi.dx(0)*E*I*phi.dx(0)*dx + d_phi*G*Ar*(phi + w.dx(0))*dx - f*d_w*dx

# newton solver
params = {"newton_solver":
	{"relative_tolerance": 1e-7, 
	 "absolute_tolerance": 1e-7}}
solve(F == 0, X, bc, solver_parameters=params)

u_, w_, phi_ = X.split(deepcopy=True)

plot(w_)
plt.title("Displacement w [mm]")
plt.show()

plot(u_)
plt.title("Displacement u [mm]")
plt.show()