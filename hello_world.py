from fenics import *
import matplotlib.pyplot as plt

mesh = UnitSquareMesh(10, 10)
V = FunctionSpace (mesh, "Lagrange", 1)

u_D = Expression('1 + x[0]*x[0] + 2*x[1]*x[1]', degree=2)

def boundary(x, on_boundary):
	return on_boundary

bc = DirichletBC(V, u_D, boundary)

u = TrialFunction(V)
v = TestFunction(V)
f = Constant(-6.0)

a = dot(grad(u), grad(v))*dx
L = f*v*dx

sol = Function(V)
solve(a == L, sol, bc)

plot(sol)
plt.show()