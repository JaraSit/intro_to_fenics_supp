from fenics import *
import matplotlib.pyplot as plt

# Variables and constants
lambda_ = 1.25
mu = 1

# Create mesh and define function space
mesh = RectangleMesh(Point(0.0, 0.0), Point(5.0, 5.0), 20, 20)
V = VectorFunctionSpace(mesh, 'P', 1)

# Define boundary condition
tol = 1e-14

def boundary(x, on_boundary):
	return (on_boundary and x[1] < tol)

bc = DirichletBC(V, Constant((0.0, 0.0)), boundary)

boundaries = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)

top = AutoSubDomain(lambda x: near(x[0], 5.0))

top.mark(boundaries, 3)
ds = ds(subdomain_data=boundaries)

# Define strain and stress
def epsilon(u):
	return 0.5*(nabla_grad(u) + nabla_grad(u).T)

def sigma(u):
	return lambda_*div(u)*Identity(d) + 2*mu*epsilon(u)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
d = u.geometric_dimension()
f = Constant((0.0, 0.0))

a = inner(sigma(u), epsilon(v))*dx
L = dot(f, v)*dx - inner(Constant((1.0, 0.0)), v)*ds(3)

# Compute solution
sol = Function(V)
solve(a == L, sol, bc)

# Plot solution
plot(sol)
plt.show()

# Save solution
file = File("plane_strain/plane_strain.pvd")
file << sol